** How to install**

* gem 'results_keeper_rspec'
* bundle install
* Add the following to your spec_helper.rb

```
#!ruby

if ENV['SEND_RK_RESULTS']
  require 'results_keeper_rspec'

  ResultsKeeperRspec.instance.send_revision

  RSpec.configure  do |config|
    config.after :each, type: :feature do |scenario|
      ResultsKeeperRspec.instance.send_test(scenario)
    end
  end

  at_exit do
    ResultsKeeperRspec.instance.send_revision_complete
  end
end
```

* Create a [Results Keeper](https://results-keeper.herokuapp.com/users/sign_up) account
* Click on Your Name link at the top
* Copy your secret key
* export the copied key (or add it to your .bashrc)

```
#!bash

export RK_SECRET_KEY=XXXXXXX-XXXXXXXXXXXX-XXXXXX

# or use 

echo "export RK_SECRET_KEY=XXXXXXX-XXXXXXXXXXXX-XXXXXX" >> ~/.bashrc 
# to add to .bashrc, and not to export all the time manually
```
**Usage**

* To send screenshots on failures
```
#!bash
SEND_RK_RESULTS=true SEND_SCREENSHOTS=true rspec spec/features

```

* To send results to a particular project (revision will be created automatically)
```
#!bash
SEND_RK_RESULTS=true PROJECT_NAME=some_project rspec spec/features

```

* To send results to a particular revision of Default project

```
#!bash
SEND_RK_RESULTS=true REVISION_NAME=some_revision rspec spec/features

```

* To send to particular project and revision

```
#!bash


SEND_RK_RESULTS=true PROJECT_NAME=some_project REVISION_NAME=some_revision rspec spec/features
```