require_relative 'string'
require_relative 'results_sender'
require 'singleton'
require 'fileutils'

class ResultsKeeperRspec
  include Singleton
  attr_reader :revision_name, :revision_project

  def initialize
    @revision_name = ENV['REVISION_NAME'] || "Result for #{Time.now}"
    @revision_project = ENV['PROJECT_NAME'] || 'Default'
  end

  def send_revision
    data = { revision: @revision_name, project: project_name }
    @revision = ResultsSender.send_json(data, 'revisions')
  end

  def send_revision_complete
    data = { revision: @revision_name, project: project_name, completed: 1 }
    ResultsSender.send_json(data, 'revisions')
  end

  def send_test(scenario)
    test_duration = Time.now - scenario.metadata[:execution_result].started_at
    @screenshot_path = save_screenshot(scenario) if ENV['SEND_SCREENSHOTS']
    status = scenario.exception ? 'failed' : 'passed'
    scenario_error = scenario.exception.message if scenario.exception
    run_path = "rspec #{scenario.location}"
    data = {
     name: "#{scenario.example_group.description} - #{scenario.description}",
     status: status,
     feature_name: scenario.example_group.description,
     run_path: run_path,
     error: scenario_error,
     revision_id: @revision['revision_id'],
     duration: test_duration
    }
    @test = ResultsSender.send_json(data, 'tests')
    ResultsSender.send_screenshot(@screenshot_path, @revision, @test) if @screenshot_path
  end

  def save_screenshot(scenario)
    if scenario.exception
      screenshot_name = "#{Time.now.to_i}_#{rand(1000..9999)}.png"
      @file_path = "tmp/screenshots/#{screenshot_name}"
      Capybara.page.save_screenshot(@file_path)
      @file_path
    end
  end

  def project_name
    ENV['PROJECT_NAME']
  end

  def console_message(text)
    puts "    RK: #{text}".blue
  end
end
