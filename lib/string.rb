class String
  def colorize(color_code)
    "\e[#{color_code}m#{self}\e[0m"
  end

  def red
    colorize(31)
  end

  def blue
    colorize(96)
  end

  def green
    colorize(92)
  end

  def yellow
    colorize(33)
  end
end
