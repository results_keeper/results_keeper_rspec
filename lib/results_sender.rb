require 'net/http'
require 'json'
require 'rest_client'
require 'pry'

class ResultsSender

  def self.send_json(body, path)
    body['secret_key'] = ENV['RK_SECRET_KEY']
    @path = "/api/#{path}"
    @body = body.to_json
    request = Net::HTTP::Post.new(@path, initheader = {'Content-Type' =>'application/json'})
    request.body = @body
    response = Net::HTTP.new(report_server_host, report_server_port).start {|http| http.request(request) }
    puts "    RK: #{response.code} - #{response.message}".blue
    result_hash = JSON.parse(response.body)
    result_hash
  end

  def self.report_server_port
    ENV['REPORT_SERVER_PORT'] || 80
  end

  def self.report_server_host
    ENV['REPORT_SERVER_HOST'] || 'results-keeper-api.herokuapp.com'
  end

  def self.send_screenshot(screenshot_path, revision, test)
    if ENV['SEND_SCREENSHOTS']
      params = { project: revision['project_id'], revision: revision['revision_id'], test: test['id'] }
      RestClient.post("http://#{report_server_host}:#{report_server_port}/api/tests/upload_screenshot",
                      :name_of_file_param => File.new(screenshot_path), :body => params)
      FileUtils.rm(screenshot_path)
    end
  end
end
