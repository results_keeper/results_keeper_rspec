Gem::Specification.new do |s|
  s.name        = 'results_keeper_rspec'
  s.version     = '0.1.2'
  s.date        = '2016-03-29'
  s.summary     = 'Result Keeper Gem For Rspec Tests'
  s.description = 'Results Keeper is a application which can help you to track and manage your Rspec test results using this gem as a client.'
  s.authors     = ['Ivan Kozakov']
  s.email       = 'ivankozakov0@gmail.com'
  s.files       = ['lib/results_keeper_rspec.rb', 'lib/string.rb']
  s.homepage    = 'https://bitbucket.org/results_keeper/results_keeper_rspec/'
  s.add_runtime_dependency('flickraw')
  s.add_runtime_dependency('rest-client')
  s.license     = 'MIT'
end
