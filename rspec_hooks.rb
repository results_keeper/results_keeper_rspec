require 'results_keeper_rspec'

ResultsKeeperRspec.instance.send_revision

After do |scenario|
  if scenario.failed?
    screenshot_name = "#{Time.now.to_i}_#{rand(1000..9999)}.png"
    @file_path = "tmp/screenshots/#{screenshot_name}"
    Capybara.page.save_screenshot(@file_path)
  end

  ResultsKeeperRspec.instance.send_test(scenario, @file_path)
end

After :all do
  ResultsKeeperRspec.instance.send_revision_complete
end
